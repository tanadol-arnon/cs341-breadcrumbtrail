require 'ruby2d'
require 'xmlsimple'
require 'csv'

set(width: 1347, height: 944, background: "blue")
img = Image.new("Map.png", x: 0, y: 0)

def find_y (lat)
    return 386125.61 -20505.02 * lat
end

def find_x (lng)
    return -1925323.24 + 19446.05 * lng
end

hash = XmlSimple.xml_in('track_data.gpx')
tracks = hash['trk'][0]['trkseg'][0]['trkpt']

tracks.each do |track|
   puts "Lat : #{track['lat'].to_f} || Lng : #{track['lon'].to_f}"
   Circle.new(x:find_x(track['lon'].to_f), y:find_y(track['lat'].to_f), radius: 5, color: "blue")
end

points = CSV.read("points.csv", headers: true, header_converters: :symbol, converters: :all)
points.each do |p|
    Circle.new(x:find_x(p[:lng]), y:find_y(p[:lat]), radius: 5, color: "red")
end


show